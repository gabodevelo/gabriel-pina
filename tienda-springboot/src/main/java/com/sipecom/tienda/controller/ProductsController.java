package com.sipecom.tienda.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.sipecom.tienda.models.ProductsModels;
import com.sipecom.tienda.services.ProductsServiceImp;

@CrossOrigin(origins = { "http://localhost:4200" })
@RestController
@RequestMapping("/api")
public class ProductsController {

	@Autowired
	private ProductsServiceImp productsService;

	@GetMapping("/carga")
	public String carga() {
		return productsService.saveList();
	}
	@GetMapping("/products")
	public List<ProductsModels> index() {
		return productsService.findAll();
	}

	@GetMapping("/products/{id}")
	public ProductsModels show(@PathVariable Long id) {
		return this.productsService.findById(id);
	}

	@PostMapping("/products")
	@ResponseStatus(HttpStatus.CREATED)
	public ProductsModels create(@RequestBody ProductsModels products) {
		this.productsService.save(products);
		return products;
	}

	@PutMapping("/products/{id}")
	@ResponseStatus(HttpStatus.CREATED)
	public ProductsModels update(@RequestBody ProductsModels products, @PathVariable Long id) {
		ProductsModels currentProducts = this.productsService.findById(id);
		currentProducts.setTitle(products.getTitle());
		currentProducts.setPrice(products.getPrice());
		currentProducts.setDescription(products.getDescription());
		currentProducts.setCategory(products.getCategory());
		currentProducts.setImage(products.getImage());
		this.productsService.save(currentProducts);
		return currentProducts;
	}

	@DeleteMapping("/products/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void delete(@PathVariable Long id) {
		ProductsModels currentProducts = this.productsService.findById(id);
		this.productsService.delete(currentProducts);
	}
	
}
