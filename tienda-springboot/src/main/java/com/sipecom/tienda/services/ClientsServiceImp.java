package com.sipecom.tienda.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sipecom.tienda.models.ClientsModels;
import com.sipecom.tienda.repository.ClientsRepository;

@Service
public class ClientsServiceImp {

	@Autowired
	private ClientsRepository clientRepository;
	
	public List<ClientsModels> findAll() {
		return (List<ClientsModels>) clientRepository.findAll();
	}

	
	public void save(ClientsModels clients) {
		clientRepository.save(clients);
	}

	public ClientsModels findById(Long id) {
		return clientRepository.findById(id).orElse(null);
	}

	
	public void delete(ClientsModels clients) {
		clientRepository.delete(clients);
	}
	
	
}
