import { Component, OnInit } from '@angular/core';
import { Products } from './products';
import { ProductService } from './products.service';


@Component({
    selector: 'app-products',
    templateUrl: './products.component.html',
    styleUrls: ['./products.component.css']
})

export class ProductsComponent implements OnInit {

    products: Products[] = [];
    
    constructor(private productService: ProductService) {

    }
    
    ngOnInit() {
        this.productService.getProducts().subscribe(
            products =>
                this.products = products
            );    
    }
    

}