import { Injectable } from "@angular/core";
import { Products } from "./products";
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs";

@Injectable()
export class ProductService {
    
    private urlEndPoint: string='http://localhost:8080/api/products';
    
    constructor(private http: HttpClient) { }

    getProducts(): Observable <Products[]> {
        return this.http.get(this.urlEndPoint).pipe(
            map((response) => response as Products[])
        );
    }
}