import { Component } from '@angular/core';

@Component({
    selector: 'app-header',
    templateUrl: './header.components.html',
    styleUrls: ['./header.components.css']
})
export class HeaderComponent {
    welcome: string = 'Bienvenido a tu tienda Virtual'
    phone: string = '+58 123-45678' 
    title: string = 'ecommerce'
    login: string = 'Login'
    logout: string = 'Logout'
    profile: string = 'Profile'
    register: string = 'Registro'

    titleslider1: string = 'Ventas Seguras'
    titleslider2: string = 'Soporte 24/7'
    titleslider3: string = 'Formas de pago'

    contentslider1: string = ''
    contentslider2: string = ''
    contentslider3: string = ''

    menu1: string = 'Home'
    menu2: string = 'Productos'
    menu3: string = 'Contactos'
}