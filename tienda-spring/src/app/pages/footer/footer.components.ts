import { Component } from '@angular/core';


@Component({
  selector: 'app-footer',
  templateUrl: './footer.components.html',
  styleUrls: ['./footer.components.css']
})

export class FooterComponent {
  'getconection': string = 'Conectate ahora con tus redes sociales:';
  'namecompany': string = 'Tienda Spring - V1';
  'desarrollo': string = 'Tienda Virtual desarrollada con fines educativos y demostrativos.';
  'articulos': string = 'Productos';
  'servicios': string = 'Servicios';
  'ubicacion': string = 'Prueba CL, VE'
  'phone': string = '+58 123 4567890';
  'email': string = 'prueba@tiendaspringv1.com';
  'copyyear': string = '© 2022 Copyright:';
  'copyright': string = 'Tienda Angular - Spring Boot - V1';
    

}